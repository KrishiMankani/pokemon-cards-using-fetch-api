document.querySelector('#search').addEventListener('click',getPokemon)

function getPokemon(e){
    const name = document.querySelector('#pokemonName').value;
    fetch(`https://pokeapi.co/api/v2/pokemon/${name}`)
    .then((response)=> response.json())
    .then(data =>{
        document.querySelector('.pokemonBox').innerHTML = 
        `
            <div>
            <img 
            src = "${data.sprites.other["official-artwork"].front_default}"
            />
            <div class = "pokemonInfo">
            <h1>${data.name}</h1>
            <h1>Weight: ${data.weight}</h1>
            </div>
        `
    })
.catch((err) => {
    document.querySelector('.pokemonBox').innerHTML = `
    <div>
        <h2>Pokemon not found</h2>
    <div>
    `
});
}

